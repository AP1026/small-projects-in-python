print("Welcome to Wario's Palace where greed is good !")
print("You have 500 coins to start.")
coins = 500 
in_game = True
while in_game:
    print("You are in the casino.")
    roulette_number = -1
    while (roulette_number < 1) or (roulette_number > 50):
        roulette_number = input("Please select a number between 1 and 50 for your game.")
        #on doit convertir car un input est de type char
        try:
            roulette_number = int(roulette_number)
        except ValueError:
            print("You have not select a number for your game.")
            roulette_number = -1
            continue
        if (roulette_number < 1) or (roulette_number > 50):
            print("Please select a correct number.")
    print("-"*50)
    print("Your selected number is : "+str(roulette_number))
    print("-"*50)
    if roulette_number % 2 == 0:
        selected_color = "red"
    else:
        selected_color = "black"
    bet_money = -1
    while (bet_money <= 1) or (bet_money > coins):
        bet_money = input("How much would you like to bet ? ")
        try:
            bet_money= int(bet_money)
        except ValueError:
            print("You have not select a sum for your bet.")
            bet_money = -1
            continue
        if bet_money <= 0:
            print("You must bet at least 1 coin.")
            continue
        if bet_money > coins:
            print("You do not have so much coins. You have "+str(coins)+" coins left.")
            continue
        print("Roulette spinning...")
        import random
        winning_number = random.randrange(1, 51)
        if winning_number % 2 == 0:
            winning_color= "red"
        else:
            winning_color= "black"
        print("The winning number is "+str(winning_number))
        print("The winning color is "+winning_color)
        if winning_number == roulette_number:
            coins = coins - bet_money
            gain = (3*bet_money)
            coins = coins + gain
            print("Congratulations ! You win ! You gain "+str(gain)+" coins.")
            print("You have "+str(coins)+" left.")
        elif winning_number != roulette_number and winning_color == selected_color:
            coins = coins - bet_money
            gain = (bet_money)/2
            coins = coins + gain
            print("Sorry. You lose. You have selected the correct color so you gain "+str(gain)+" coins.")
            print("You have "+str(coins)+" left.")
        else:
            coins = coins - bet_money
            print("Sorry. You lose. You lost "+str(bet_money)+" coins.")
            print("You have "+str(coins)+" left.")
    if coins <= 0:
        print("You have no coins left. We hope you enjoy your stay. Have a nice day !")
        in_game = False
    else:
        print("You have "+str(coins)+" left.")
        leave = input("Would you like to leave the casino ? [Y]")
        if(leave == "Y") or (leave == "y") or (leave == "1"):
            in_game = False
        else:
            in_game = True
            