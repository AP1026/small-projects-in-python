
# 
LOOK_AT = ["up", "right", "down", "left"]
ACTIONS_PER_TURN = 1
POSSIBLE_ACTIONS = ["turn left", "turn right", "run"]

class SuperHero:
    # ceci est une variable de classe
    # chaque instance de la classe à la même valeur
    MAX_POWER = 100
    
    def __init__(self, name, can_fly=False, has_car=False):
        # on garde le nom du super hero
        self.name = name
        
        self.can_fly = can_fly
        self.has_car = has_car
        
        # le power initial est la power max de la classe SuperHero
        self.power = SuperHero.MAX_POWER
        
        # on a aussi besoin de la position du super heroe
        self.position = (None, None)

        # on initialise aléatoirement la direction dans laquelle il regarde
        self._look_at_index = randrange(4)
        self.look_at  = LOOK_AT[self._look_at_index]
        
        # on a besoin des 3 prochaines actions
        self.next_actions = []
        
    def __str__(self):
        return "I'm {} at position {}, {}, looking at {}, power {}".format(
            self.name,
            self.position[0],
            self.position[1],
            self.look_at,
            self.power
        )
    
    def get_label(self):
        """Retourne le label du super hero
        
        Le label c'est les 3 premiers caractères du nom.
        
        Return
            (str): le label du super hero
        """
        return self.name[0:3]
    
    def _ask_for_valid_action(self):
        action_is_valid = False
        while not action_is_valid:
            # on demande quelle action faire
            action = input("Prochaine action ? ")
            
            # on vérifie que l'action retournée est bien disponible
            if action in POSSIBLE_ACTIONS:
                action_is_valid = True
        return action
    
    def ask_for_next_actions(self):
        # on boucle pour demander les 3 prochaines actions
        for k in range(ACTIONS_PER_TURN):
            next_action = self._ask_for_valid_action()
            self.next_actions.append(next_action)
    
    def reset_next_actions(self):
        self.next_actions = []
    
    #-------------------------
    # A partir d'ici on définit les actions du super hero
    
    def turn_left(self):
        self._look_at_index = (self._look_at_index - 1) % 4
        self.look_at = LOOK_AT[self._look_at_index]
    
    def turn_right(self):
        self._look_at_index = (self._look_at_index + 1) % 4
        self.look_at = LOOK_AT[self._look_at_index]
    
    def run(self):
        """On avance d'une case dans la direction dans laquelle on regarde"""
        x_position = self.position[0]
        y_position = self.position[1]
        if self.look_at == "up":
            new_position = (x_position, y_position - 1)
        elif self.look_at == "down":
            new_position = (x_position, y_position + 1)
        elif self.look_at == "left":
            new_position = (x_position - 1, y_position)
        elif self.look_at == "right":
            new_position = (x_position + 1, y_position)
        else:
            raise Exception("Probleme dans look at")
        self.position = new_position
        

AVAILABLE_HEROES = [
    SuperHero("Batman", can_fly=False, has_car=True),
    SuperHero("Superman", can_fly=True, has_car=False),
]
    
    
batman = SuperHero("Batman")
superman = SuperHero("Superman")

batman.get_label()

print(batman)
batman.turn_left()
print(batman)

print(superman)


import math
from random import randrange


class Board:
    
    def __init__(self, x=10, y=10):
        print("New board")
        
        # on a besoin de la taille du plateau
        self.x = x
        self.y = y
        
        # la carte qui dit si on peut aller dans la case ou pas
        self.map = {}
        
        # on construit la carte
        self.init_map()
    
    def __str__(self):
        return "Board size: {}, {}".format(self.x, self.y)
    
    def _draw_valid_cell(self):
        """Dessine une cellule invalide
        """
        return "___"
    
    def _draw_invalid_cell(self):
        return "###"
    
    def draw_debug(self):
        for (position, cell) in self.map.items():
            if cell is True:
                cell_texture = self._draw_valid_cell()
            else:
                cell_texture = self._draw_invalid_cell()
            print("cellule {}: {}".format(position, cell_texture))
    
    def draw(self, players_positions={}):
        """Dessine le plateau de jeu
        
        Dessine les cellules
        Dessine les superhéros
        
        Arguments:
            - players_positions (dict): optionnel, les positions des joueurs
                {(0, 1): "Bat", (3, 4): "Sup"}
        """
        for j in range(self.y):
            # on créer une ligne avec que des textures de cellule vide
            # la multiplication d'une liste par un entier concatène
            # n fois la liste
            line = [self._draw_valid_cell()] * self.x
            
            # on boucle sur la ligne
            # on remplace la cellule valide par une invalide en fonction du
            # plateau de jeu self.map
            for i in range(self.x):
                
                # la position de la cellule: i, j
                position = (i, j)
                
                # si à cette position on a un obstacle
                # on dessine une cellule invalide
                if self.map[position] is False:
                    line[i] = self._draw_invalid_cell()
                
                # on vérifie si un joueur est dans la cellule
                player_in_this_cell = position in players_positions
                
                # si c'est le cas: on dessine le joueur
                if player_in_this_cell is True:
                    line[i] = players_positions[position]
            
            # on construit la ligne avec des caractères spéciaux
            print("|".join(line))
            
    def get_valid_random_position(self):
        """Retourne une position valide aléatoirement
        
        Valide == sur une case True
        
        Return:
            (tuple): la position valide aléatoire
        """
        # on veut trouver une cellule valide:
        # tant qu'on a pas trouver on recommence
        selected_cell_is_valid = False
        while not selected_cell_is_valid:
            # on tire aléatoirement x et y
            random_x = randrange(0, self.x)
            random_y = randrange(0, self.y)
            random_position = (random_x, random_y)
            
            # si la cellule aléatoire est valide: on sort de la boucle
            if self.map[random_position] is True:
                selected_cell_is_valid = True
                
        # on retourne les coordonnées d'une cellule valide
        return random_position
    
    def init_map(self):
        """Construit le plateau de jeu"""
        print("Building map...")
        
        for i in range(self.x):
            for j in range(self.y):
                # le tuple de la position dans la grille du plateau
                position = (i, j)
                
                # on modifie le plateau: on met tout à True pour le moment
                self.map[position] = True
        
        # Placer aléatoirement des cases à False
        # 10% du plateau en False
        n_invalid_cells = math.floor(0.1 * self.x * self.y)
        print("cellules invalides:", n_invalid_cells)
        
        # Boucle sur les n cellules invalides
        # On tire aléatoirement i et j
        # On passe le plateau à False sur les cellules invalides
        for k in range(n_invalid_cells):
            x_position = randrange(0, self.x)
            y_position = randrange(0, self.y)
            position = (x_position, y_position)
            self.map[position] = False
        
        
        print("Map done...")
    

class Game:
    def __init__(self, board, players):
        print("New game")
        
        # on a besoin d'un plateau de jeu
        self.board = board
        
        # on a besoin de joueurs
        self.players = players
        
        # on initialise les joueurs
        self._init_players()
    
    def _init_players(self):
        """Initialise les joueurs
        
        On positionne les joueurs aléatoirement sur le board
        """
        for player in self.players:
            player.position = self.board.get_valid_random_position()
    
    def get_all_players_positions(self):
        # on a besoin d'un dico
        # clef: position du joueur
        # valeur: la texture du joueur (le label get_label())
        players_positions = {}
        for player in self.players:
            
            # on récupère la position et le label du héro
            position = player.position
            label    = player.get_label()
            
            # on stocke les infos dans le dico attendu par board.draw()
            players_positions[position] = label
        return players_positions
    
    def update_board(self):
        """Met à jour l'affichage du jeu
        
        - met à a jour le plateau
        - affiche l'état de chaque héro
        """
        self.board.draw(self.get_all_players_positions())
        
        for player in self.players:
            print(player)
    
    def start(self):
        print("Start the game")
        
        
        stay_in_game = True
        while stay_in_game:
            
            # on affiche le plateau
            self.update_board()
            
            # on demande aux joueurs ce qu'ils veulent faire
            for player in self.players:
                print("{} it's your turn !".format(player.name))
                player.ask_for_next_actions()
            
            
            
            # on résoud les actions des joueurs
            actions = []
            for k in range(ACTIONS_PER_TURN):
                for player in self.players:
                    # on récupère la prochaine action
                    action_name = player.next_actions[k]
                    print("next action", action_name)
                    
                    # on en déduit la méthode à appeler
                    action_func = getattr(player, action_name.replace(" ", "_"))
                    
                    # on execute l'action
                    action_func()
            
            
            # on redessine le plateau
            self.update_board()
            
            
            
            
            stay_in_game = False

board = Board(x=10, y=10)
print(board)

players = [
    batman,
    superman
]

game = Game(board, players)
game.start()